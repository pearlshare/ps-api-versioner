var expect = require("expect.js"),
    apiVersioner = require("../"),
    res = {},
    versioner = apiVersioner(1);

function buildReq(){
  return {
    headers: {},
    get: function(item){
      return this.headers[item] || this.headers[item.toLowerCase()];
    }
  };
}

describe("ps-api-versioner", function () {
  it("should set the default values", function () {
    var req = buildReq();
    versioner(req, res, function(){
      expect(req.get("accept-version")).to.equal(1);
      expect(req.get("content-type-version")).to.equal(1);
    });
  });

  it("should set the accept headers", function () {
    var req = buildReq();
    req.headers["accept"] = "application/vnd.pearlshare.v2+json";
    versioner(req, res, function(){
      expect(req.get("accept-version")).to.equal(2);
      expect(req.get("accept-namespace")).to.equal("pearlshare");
      expect(req.get("accept")).to.equal("application/json");
    });
  });

  it("should set the accept headers", function () {
    var req = buildReq();
    req.headers["content-type"] = "text/vnd.fish.v3+xml";
    versioner(req, res, function(){
      expect(req.get("content-type-version")).to.equal(3);
      expect(req.get("content-type-namespace")).to.equal("fish");
      expect(req.get("content-type")).to.equal("text/xml");
    });
  });

  it("should set the accept headers", function () {
    var req = buildReq();
    req.headers["accept"] = "application/vnd.pearlshare.v2+json,text/csv";
    versioner(req, res, function(){
      expect(req.get("accept-version")).to.equal(2);
      expect(req.get("accept-namespace")).to.equal("pearlshare");
      expect(req.get("accept")).to.equal("application/json");
    });
  });
});