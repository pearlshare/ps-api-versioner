# API Versioner

Process client api request versions

## What is it?

Sets 'accept-version' and 'content-version' headers based on the 'Accept' and 'Content-Type' headers being in the format of:

    application/vnd.[namespace].v[version]+[format]

* namespace is a word such as 'pearlshare'
* version is a number such as '1' or '2.4'
* format is the mime type such as 'json' or 'xml'

With the following request header:

    Content-Type: application/vnd.pearlshare.v1+json

These will be set to:

* req.headers['content-type-namespace'] = 'pearlshare'
* req.headers['content-type-version'] = '1'
* req.headers['content-type'] = 'application/json'

## Usage

```javascript
    var express = require('express'),
        apiVersion = require('ps-api-version'),
        app = express();

    app.use(apiVersion(1));
    app.get('/', function(req, res){
      console.log(req.headers['content-version']) // => '1'
    });
```

## Developing

Via PR. Test using `npm test`

## Licence

MIT