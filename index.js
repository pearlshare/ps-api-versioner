module.exports = function(defaultApiVersion){
  if (!defaultApiVersion) throw(new Error("no defaultApiVersion provided"));

  return function apiVerionMiddleware(req, res, next) {
    var versionRegex = /^(application|text)\/vnd\.(\w+)\.v([\d\.]*)\+(\w+)$/;
    req.headers["accept-version"] = req.headers["content-type-version"] = defaultApiVersion;

    if (req.get("Accept")) {
      var options = req.get("Accept").split(","); // May be multiple types allowed

      for (var i = 0; i < options.length; i++) {
        var acceptResults = options[i].match(versionRegex);
        if (acceptResults) {
          req.headers["accept-namespace"] = acceptResults[2];
          req.headers["accept-version"] = parseFloat(acceptResults[3]);
          req.headers["accept"] = acceptResults[1] + "/" + acceptResults[4];
          break; // Accept first match
        }
      }
    }

    if (req.get("Content-Type")) {
      var options = req.get("Content-Type").split(",");

      for (var i = 0; i < options.length; i++) {
        var contentTypeResults = options[i].match(versionRegex);
        if (contentTypeResults) {
          req.headers["content-type-namespace"] = contentTypeResults[2];
          req.headers["content-type-version"] = parseFloat(contentTypeResults[3]);
          req.headers["content-type"] = contentTypeResults[1] + "/" + contentTypeResults[4];
          break;
        }
      }
    }

    next();
  };
};
